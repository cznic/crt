# crt

Package crt provides C-runtime services. Work In Progress. API unstable.

Installation

    $ go get modernc.org/crt

Documentation: [godoc.org/modernc.org/crt](http://godoc.org/modernc.org/crt)

Building with `make` requires the following Go packages

* github.com/golang/lint/golint
* github.com/mdempsky/maligned
* github.com/mdempsky/unconvert
* honnef.co/go/tools/cmd/unused
* honnef.co/go/tools/cmd/gosimple
* github.com/client9/misspell/cmd/misspell

// Copyright 2019 The CRT Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

// +build amd64,!windows

package crt // import "modernc.org/crt/v3"

import (
	"math"
	"strings"

	"modernc.org/crt/v3/libc/sys/mman"
)

const (
	maxInt   = math.MaxInt32
	maxLong  = math.MaxInt64
	maxUlong = math.MaxUint64
	minInt   = math.MinInt32
	minLong  = math.MinInt64
)

type (
	Intptr  = int64
	RawMem  [1<<50 - 1]byte
	Size_t  = uint64
	Ssize_t = int64
	dev_t   = uint64
	long    = int64
	ulong   = uint64
)

type bits []int

func newBits(n int) (r bits)  { return make(bits, (n+63)>>6) }
func (b bits) has(n int) bool { return b != nil && b[n>>6]&(1<<uint(n&63)) != 0 }
func (b bits) set(n int)      { b[n>>6] |= 1 << uint(n&63) }

func mmanFlags(n int32) string {
	var a []string
	if n&mman.MAP_32BIT != 0 {
		a = append(a, "MAP_32BIT")
	}
	if n&mman.MAP_ANON != 0 {
		a = append(a, "MAP_ANON")
	}
	if n&mman.MAP_ANONYMOUS != 0 {
		a = append(a, "MAP_ANONYMOUS")
	}
	if n&mman.MAP_DENYWRITE != 0 {
		a = append(a, "MAP_DENYWRITE")
	}
	if n&mman.MAP_EXECUTABLE != 0 {
		a = append(a, "MAP_EXECUTABLE")
	}
	if n&mman.MAP_FILE != 0 {
		a = append(a, "MAP_FILE")
	}
	if n&mman.MAP_FIXED != 0 {
		a = append(a, "MAP_FIXED")
	}
	if n&mman.MAP_GROWSDOWN != 0 {
		a = append(a, "MAP_GROWSDOWN")
	}
	if n&mman.MAP_HUGETLB != 0 {
		a = append(a, "MAP_HUGETLB")
	}
	if n&mman.MAP_HUGE_MASK != 0 {
		a = append(a, "MAP_HUGE_MASK")
	}
	if n&mman.MAP_HUGE_SHIFT != 0 {
		a = append(a, "MAP_HUGE_SHIFT")
	}
	if n&mman.MAP_LOCKED != 0 {
		a = append(a, "MAP_LOCKED")
	}
	if n&mman.MAP_NONBLOCK != 0 {
		a = append(a, "MAP_NONBLOCK")
	}
	if n&mman.MAP_NORESERVE != 0 {
		a = append(a, "MAP_NORESERVE")
	}
	if n&mman.MAP_POPULATE != 0 {
		a = append(a, "MAP_POPULATE")
	}
	if n&mman.MAP_PRIVATE != 0 {
		a = append(a, "MAP_PRIVATE")
	}
	if n&mman.MAP_SHARED != 0 {
		a = append(a, "MAP_SHARED")
	}
	if n&mman.MAP_SHARED_VALIDATE != 0 {
		a = append(a, "MAP_SHARED_VALIDATE")
	}
	if n&mman.MAP_STACK != 0 {
		a = append(a, "MAP_STACK")
	}
	if n&mman.MAP_SYNC != 0 {
		a = append(a, "MAP_SYNC")
	}
	if n&mman.MAP_TYPE != 0 {
		a = append(a, "MAP_TYPE")
	}
	return strings.Join(a, "|")
}

// Code generated by 'ccgo /tmp/go-generate-358883214/x.c -ccgo-crt-import-path  -ccgo-export-defines  -ccgo-export-enums  -ccgo-export-externs X -ccgo-export-fields F -ccgo-export-structs  -ccgo-export-typedefs  -ccgo-pkgname time -o libc/time/time_linux_amd64.go', DO NOT EDIT.

package time

import (
	"math"
	"reflect"
	"unsafe"
)

var _ = math.Pi
var _ reflect.Kind
var _ unsafe.Pointer

const (
	CLOCK_BOOTTIME            = 7
	CLOCK_BOOTTIME_ALARM      = 9
	CLOCK_MONOTONIC           = 1
	CLOCK_MONOTONIC_COARSE    = 6
	CLOCK_MONOTONIC_RAW       = 4
	CLOCK_PROCESS_CPUTIME_ID  = 2
	CLOCK_REALTIME            = 0
	CLOCK_REALTIME_ALARM      = 8
	CLOCK_REALTIME_COARSE     = 5
	CLOCK_TAI                 = 11
	CLOCK_THREAD_CPUTIME_ID   = 3
	TIMER_ABSTIME             = 1
	TIME_UTC                  = 1
	X_ATFILE_SOURCE           = 1
	X_BITS_TIME_H             = 1
	X_BITS_TYPESIZES_H        = 1
	X_BITS_TYPES_H            = 1
	X_BITS_TYPES_LOCALE_T_H   = 1
	X_BITS_TYPES___LOCALE_T_H = 1
	X_BSD_SIZE_T_             = 0
	X_BSD_SIZE_T_DEFINED_     = 0
	X_DEFAULT_SOURCE          = 1
	X_FEATURES_H              = 1
	X_GCC_SIZE_T              = 0
	X_LP64                    = 1
	X_POSIX_C_SOURCE          = 200809
	X_POSIX_SOURCE            = 1
	X_SIZET_                  = 0
	X_SIZE_T                  = 0
	X_SIZE_T_                 = 0
	X_SIZE_T_DECLARED         = 0
	X_SIZE_T_DEFINED          = 0
	X_SIZE_T_DEFINED_         = 0
	X_STDC_PREDEF_H           = 1
	X_STRUCT_TIMESPEC         = 1
	X_SYS_CDEFS_H             = 1
	X_SYS_SIZE_T_H            = 0
	X_TIME_H                  = 1
	X_T_SIZE                  = 0
	X_T_SIZE_                 = 0
	Linux                     = 1
	Unix                      = 1
)

type Ptrdiff_t = int64 /* <builtin>:3:26 */

type Size_t = uint64 /* <builtin>:9:23 */

type Wchar_t = int32 /* <builtin>:15:24 */

// ISO/IEC 9899:1999 7.23.1: Components of time
//   The macro `CLOCKS_PER_SEC' is an expression with type `clock_t' that is
//   the number per second of the value returned by the `clock' function.
// CAE XSH, Issue 4, Version 2: <time.h>
//   The value of CLOCKS_PER_SEC is required to be 1 million on all
//   XSI-conformant systems.

// Identifier for system-wide realtime clock.
// Monotonic system-wide clock.
// High-resolution timer from the CPU.
// Thread-specific CPU-time clock.
// Monotonic system-wide clock, not adjusted for frequency scaling.
// Identifier for system-wide realtime clock, updated only on ticks.
// Monotonic system-wide clock, updated only on ticks.
// Monotonic system-wide clock that includes time spent in suspension.
// Like CLOCK_REALTIME but also wakes suspended system.
// Like CLOCK_BOOTTIME but also wakes suspended system.
// Like CLOCK_REALTIME but in International Atomic Time.

// Flag to indicate time is absolute.

// Many of the typedefs and structs whose official home is this header
//   may also need to be defined by other headers.

// bits/types.h -- definitions of __*_t types underlying *_t types.
//   Copyright (C) 2002-2018 Free Software Foundation, Inc.
//   This file is part of the GNU C Library.
//
//   The GNU C Library is free software; you can redistribute it and/or
//   modify it under the terms of the GNU Lesser General Public
//   License as published by the Free Software Foundation; either
//   version 2.1 of the License, or (at your option) any later version.
//
//   The GNU C Library is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//   Lesser General Public License for more details.
//
//   You should have received a copy of the GNU Lesser General Public
//   License along with the GNU C Library; if not, see
//   <http://www.gnu.org/licenses/>.

// Never include this file directly; use <sys/types.h> instead.

// Returned by `clock'.
type Clock_t = int64 /* clock_t.h:7:19 */

// bits/types.h -- definitions of __*_t types underlying *_t types.
//   Copyright (C) 2002-2018 Free Software Foundation, Inc.
//   This file is part of the GNU C Library.
//
//   The GNU C Library is free software; you can redistribute it and/or
//   modify it under the terms of the GNU Lesser General Public
//   License as published by the Free Software Foundation; either
//   version 2.1 of the License, or (at your option) any later version.
//
//   The GNU C Library is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//   Lesser General Public License for more details.
//
//   You should have received a copy of the GNU Lesser General Public
//   License along with the GNU C Library; if not, see
//   <http://www.gnu.org/licenses/>.

// Never include this file directly; use <sys/types.h> instead.

// Returned by `time'.
type Time_t = int64 /* time_t.h:7:18 */

// bits/types.h -- definitions of __*_t types underlying *_t types.
//   Copyright (C) 2002-2018 Free Software Foundation, Inc.
//   This file is part of the GNU C Library.
//
//   The GNU C Library is free software; you can redistribute it and/or
//   modify it under the terms of the GNU Lesser General Public
//   License as published by the Free Software Foundation; either
//   version 2.1 of the License, or (at your option) any later version.
//
//   The GNU C Library is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//   Lesser General Public License for more details.
//
//   You should have received a copy of the GNU Lesser General Public
//   License along with the GNU C Library; if not, see
//   <http://www.gnu.org/licenses/>.

// Never include this file directly; use <sys/types.h> instead.

// ISO C `broken-down time' structure.
type Tm = struct {
	Ftm_sec    int32
	Ftm_min    int32
	Ftm_hour   int32
	Ftm_mday   int32
	Ftm_mon    int32
	Ftm_year   int32
	Ftm_wday   int32
	Ftm_yday   int32
	Ftm_isdst  int32
	Ftm_gmtoff int64
	Ftm_zone   uintptr
}

// NB: Include guard matches what <linux/time.h> uses.

// bits/types.h -- definitions of __*_t types underlying *_t types.
//   Copyright (C) 2002-2018 Free Software Foundation, Inc.
//   This file is part of the GNU C Library.
//
//   The GNU C Library is free software; you can redistribute it and/or
//   modify it under the terms of the GNU Lesser General Public
//   License as published by the Free Software Foundation; either
//   version 2.1 of the License, or (at your option) any later version.
//
//   The GNU C Library is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//   Lesser General Public License for more details.
//
//   You should have received a copy of the GNU Lesser General Public
//   License along with the GNU C Library; if not, see
//   <http://www.gnu.org/licenses/>.

// Never include this file directly; use <sys/types.h> instead.

// POSIX.1b structure for a time value.  This is like a `struct timeval' but
//   has nanoseconds instead of microseconds.
type Timespec = struct {
	Ftv_sec  int64
	Ftv_nsec int64
}

// bits/types.h -- definitions of __*_t types underlying *_t types.
//   Copyright (C) 2002-2018 Free Software Foundation, Inc.
//   This file is part of the GNU C Library.
//
//   The GNU C Library is free software; you can redistribute it and/or
//   modify it under the terms of the GNU Lesser General Public
//   License as published by the Free Software Foundation; either
//   version 2.1 of the License, or (at your option) any later version.
//
//   The GNU C Library is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//   Lesser General Public License for more details.
//
//   You should have received a copy of the GNU Lesser General Public
//   License along with the GNU C Library; if not, see
//   <http://www.gnu.org/licenses/>.

// Never include this file directly; use <sys/types.h> instead.

// Clock ID used in clock and timer functions.
type Clockid_t = int32 /* clockid_t.h:7:21 */

// bits/types.h -- definitions of __*_t types underlying *_t types.
//   Copyright (C) 2002-2018 Free Software Foundation, Inc.
//   This file is part of the GNU C Library.
//
//   The GNU C Library is free software; you can redistribute it and/or
//   modify it under the terms of the GNU Lesser General Public
//   License as published by the Free Software Foundation; either
//   version 2.1 of the License, or (at your option) any later version.
//
//   The GNU C Library is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//   Lesser General Public License for more details.
//
//   You should have received a copy of the GNU Lesser General Public
//   License along with the GNU C Library; if not, see
//   <http://www.gnu.org/licenses/>.

// Never include this file directly; use <sys/types.h> instead.

// Timer ID returned by `timer_create'.
type Timer_t = uintptr /* timer_t.h:7:19 */

// bits/types.h -- definitions of __*_t types underlying *_t types.
//   Copyright (C) 2002-2018 Free Software Foundation, Inc.
//   This file is part of the GNU C Library.
//
//   The GNU C Library is free software; you can redistribute it and/or
//   modify it under the terms of the GNU Lesser General Public
//   License as published by the Free Software Foundation; either
//   version 2.1 of the License, or (at your option) any later version.
//
//   The GNU C Library is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//   Lesser General Public License for more details.
//
//   You should have received a copy of the GNU Lesser General Public
//   License along with the GNU C Library; if not, see
//   <http://www.gnu.org/licenses/>.

// Never include this file directly; use <sys/types.h> instead.

// NB: Include guard matches what <linux/time.h> uses.

// POSIX.1b structure for timer start values and intervals.
type Itimerspec = struct {
	Fit_interval struct {
		Ftv_sec  int64
		Ftv_nsec int64
	}
	Fit_value struct {
		Ftv_sec  int64
		Ftv_nsec int64
	}
}

type Pid_t = int32 /* time.h:54:17 */

// Definition of locale_t.
//   Copyright (C) 2017-2018 Free Software Foundation, Inc.
//   This file is part of the GNU C Library.
//
//   The GNU C Library is free software; you can redistribute it and/or
//   modify it under the terms of the GNU Lesser General Public
//   License as published by the Free Software Foundation; either
//   version 2.1 of the License, or (at your option) any later version.
//
//   The GNU C Library is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//   Lesser General Public License for more details.
//
//   You should have received a copy of the GNU Lesser General Public
//   License along with the GNU C Library; if not, see
//   <http://www.gnu.org/licenses/>.

// Definition of struct __locale_struct and __locale_t.
//   Copyright (C) 1997-2018 Free Software Foundation, Inc.
//   This file is part of the GNU C Library.
//   Contributed by Ulrich Drepper <drepper@cygnus.com>, 1997.
//
//   The GNU C Library is free software; you can redistribute it and/or
//   modify it under the terms of the GNU Lesser General Public
//   License as published by the Free Software Foundation; either
//   version 2.1 of the License, or (at your option) any later version.
//
//   The GNU C Library is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//   Lesser General Public License for more details.
//
//   You should have received a copy of the GNU Lesser General Public
//   License along with the GNU C Library; if not, see
//   <http://www.gnu.org/licenses/>.

// POSIX.1-2008: the locale_t type, representing a locale context
//   (implementation-namespace version).  This type should be treated
//   as opaque by applications; some details are exposed for the sake of
//   efficiency in e.g. ctype functions.

type X__locale_struct = struct {
	F__locales       [13]uintptr
	F__ctype_b       uintptr
	F__ctype_tolower uintptr
	F__ctype_toupper uintptr
	F__names         [13]uintptr
}

type Locale_t = uintptr /* locale_t.h:24:20 */

var _ int8 /* x.c:2:13: */

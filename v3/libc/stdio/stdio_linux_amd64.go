// Code generated by 'ccgo /tmp/go-generate-358883214/x.c -ccgo-crt-import-path  -ccgo-export-defines  -ccgo-export-enums  -ccgo-export-externs X -ccgo-export-fields F -ccgo-export-structs  -ccgo-export-typedefs  -ccgo-pkgname stdio -o libc/stdio/stdio_linux_amd64.go', DO NOT EDIT.

package stdio

import (
	"math"
	"reflect"
	"unsafe"
)

var _ = math.Pi
var _ reflect.Kind
var _ unsafe.Pointer

const (
	BUFSIZ                = 8192
	EOF                   = -1
	FILENAME_MAX          = 4096
	FOPEN_MAX             = 16
	L_ctermid             = 9
	L_tmpnam              = 20
	P_tmpdir              = "/tmp"
	SEEK_CUR              = 1
	SEEK_END              = 2
	SEEK_SET              = 0
	TMP_MAX               = 238328
	X_ANSI_STDARG_H_      = 0
	X_ATFILE_SOURCE       = 1
	X_BITS_STDIO_LIM_H    = 1
	X_BITS_TYPESIZES_H    = 1
	X_BITS_TYPES_H        = 1
	X_BSD_SIZE_T_         = 0
	X_BSD_SIZE_T_DEFINED_ = 0
	X_DEFAULT_SOURCE      = 1
	X_FEATURES_H          = 1
	X_GCC_SIZE_T          = 0
	X_IOFBF               = 0
	X_IOLBF               = 1
	X_IONBF               = 2
	X_IO_EOF_SEEN         = 0x0010
	X_IO_ERR_SEEN         = 0x0020
	X_IO_USER_LOCK        = 0x8000
	X_LP64                = 1
	X_POSIX_C_SOURCE      = 200809
	X_POSIX_SOURCE        = 1
	X_SIZET_              = 0
	X_SIZE_T              = 0
	X_SIZE_T_             = 0
	X_SIZE_T_DECLARED     = 0
	X_SIZE_T_DEFINED      = 0
	X_SIZE_T_DEFINED_     = 0
	X_STDARG_H            = 0
	X_STDC_PREDEF_H       = 1
	X_STDIO_H             = 1
	X_SYS_CDEFS_H         = 1
	X_SYS_SIZE_T_H        = 0
	X_T_SIZE              = 0
	X_T_SIZE_             = 0
	X_VA_LIST             = 0
	X_VA_LIST_            = 0
	X_VA_LIST_DEFINED     = 0
	X_VA_LIST_T_H         = 0
	Linux                 = 1
	Unix                  = 1
)

type Ptrdiff_t = int64 /* <builtin>:3:26 */

type Size_t = uint64 /* <builtin>:9:23 */

type Wchar_t = int32 /* <builtin>:15:24 */

// The tag name of this struct is _G_fpos_t to preserve historic
//   C++ mangled names for functions taking fpos_t arguments.
//   That name should not be used in new code.
type X_G_fpos_t = struct {
	F__pos   int64
	F__state struct {
		F__count int32
		F__value struct{ F__wch uint32 }
	}
}

// bits/types.h -- definitions of __*_t types underlying *_t types.
//   Copyright (C) 2002-2018 Free Software Foundation, Inc.
//   This file is part of the GNU C Library.
//
//   The GNU C Library is free software; you can redistribute it and/or
//   modify it under the terms of the GNU Lesser General Public
//   License as published by the Free Software Foundation; either
//   version 2.1 of the License, or (at your option) any later version.
//
//   The GNU C Library is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//   Lesser General Public License for more details.
//
//   You should have received a copy of the GNU Lesser General Public
//   License along with the GNU C Library; if not, see
//   <http://www.gnu.org/licenses/>.

// Never include this file directly; use <sys/types.h> instead.

// The tag name of this struct is _G_fpos64_t to preserve historic
//   C++ mangled names for functions taking fpos_t and/or fpos64_t
//   arguments.  That name should not be used in new code.
type X_G_fpos64_t = struct {
	F__pos   int64
	F__state struct {
		F__count int32
		F__value struct{ F__wch uint32 }
	}
}

type X_IO_FILE = struct {
	F_flags          int32
	F_IO_read_ptr    uintptr
	F_IO_read_end    uintptr
	F_IO_read_base   uintptr
	F_IO_write_base  uintptr
	F_IO_write_ptr   uintptr
	F_IO_write_end   uintptr
	F_IO_buf_base    uintptr
	F_IO_buf_end     uintptr
	F_IO_save_base   uintptr
	F_IO_backup_base uintptr
	F_IO_save_end    uintptr
	F_markers        uintptr
	F_chain          uintptr
	F_fileno         int32
	F_flags2         int32
	F_old_offset     int64
	F_cur_column     uint16
	F_vtable_offset  int8
	F_shortbuf       [1]int8
	F_lock           uintptr
	F_offset         int64
	F_codecvt        uintptr
	F_wide_data      uintptr
	F_freeres_list   uintptr
	F_freeres_buf    uintptr
	F__pad5          Size_t
	F_mode           int32
	F_unused2        [20]int8
}

// The opaque type of streams.  This is the definition used elsewhere.
type FILE = X_IO_FILE /* FILE.h:7:25 */

// These macros are used by bits/stdio.h and internal headers.

// Many more flag bits are defined internally.

// Copyright (C) 1989-2018 Free Software Foundation, Inc.
//
//This file is part of GCC.
//
//GCC is free software; you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation; either version 3, or (at your option)
//any later version.
//
//GCC is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//Under Section 7 of GPL version 3, you are granted additional
//permissions described in the GCC Runtime Library Exception, version
//3.1, as published by the Free Software Foundation.
//
//You should have received a copy of the GNU General Public License and
//a copy of the GCC Runtime Library Exception along with this program;
//see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see
//<http://www.gnu.org/licenses/>.

// ISO C Standard:  7.15  Variable arguments  <stdarg.h>

// Define __gnuc_va_list.

// Define the standard macros for the user,
//   if this invocation was from the user program.

// Define va_list, if desired, from __gnuc_va_list.
// We deliberately do not define va_list when called from
//   stdio.h, because ANSI C says that stdio.h is not supposed to define
//   va_list.  stdio.h needs to have access to that data type,
//   but must not use that name.  It should use the name __gnuc_va_list,
//   which is safe because it is reserved for the implementation.

// The macro _VA_LIST_ is the same thing used by this file in Ultrix.
//   But on BSD NET2 we must not test or define or undef it.
//   (Note that the comments in NET 2's ansi.h
//   are incorrect for _VA_LIST_--see stdio.h!)
// The macro _VA_LIST_DEFINED is used in Windows NT 3.5
// The macro _VA_LIST is used in SCO Unix 3.2.
// The macro _VA_LIST_T_H is used in the Bull dpx2
// The macro __va_list__ is used by BeOS.
type Va_list = uintptr /* stdarg.h:99:24 */

type Off_t = int64 /* stdio.h:63:17 */

type Ssize_t = int64 /* stdio.h:77:19 */

// The type of the second argument to `fgetpos' and `fsetpos'.
type Fpos_t = X_G_fpos_t /* stdio.h:84:18 */

// If we are compiling with optimizing read this file.  It contains
//   several optimizing inline functions and macros.

var _ int8 /* x.c:2:13: */

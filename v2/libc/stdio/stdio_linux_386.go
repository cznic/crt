// Code generated by `gocc /tmp/go-generate-570643544/x.c -o libc/stdio/stdio_linux_386.go -qbec-defines -qbec-enumconsts -qbec-import <none> -qbec-pkgname stdio -qbec-structs`, DO NOT EDIT.

package stdio

const (
	DBUFSIZ                           = 8192
	DEOF                              = -1
	DFILENAME_MAX                     = 4096
	DFOPEN_MAX                        = 16
	DL_ctermid                        = 9
	DL_tmpnam                         = 20
	DSEEK_CUR                         = 1
	DSEEK_END                         = 2
	DSEEK_SET                         = 0
	DTMP_MAX                          = 238328
	D_ANSI_STDARG_H_                  = 0
	D_ATFILE_SOURCE                   = 1
	D_BITS_G_CONFIG_H                 = 1
	D_BITS_LIBIO_H                    = 1
	D_BITS_STDIO_LIM_H                = 1
	D_BITS_TYPESIZES_H                = 1
	D_BITS_TYPES_H                    = 1
	D_BSD_SIZE_T_                     = 0
	D_BSD_SIZE_T_DEFINED_             = 0
	D_DEFAULT_SOURCE                  = 1
	D_FEATURES_H                      = 1
	D_GCC_SIZE_T                      = 0
	D_G_BUFSIZ                        = 8192
	D_G_HAVE_MMAP                     = 1
	D_G_HAVE_MREMAP                   = 1
	D_G_HAVE_ST_BLKSIZE               = 0
	D_G_IO_IO_FILE_VERSION            = 131073
	D_IOFBF                           = 0
	D_IOLBF                           = 1
	D_IONBF                           = 2
	D_IOS_APPEND                      = 8
	D_IOS_ATEND                       = 4
	D_IOS_BIN                         = 128
	D_IOS_INPUT                       = 1
	D_IOS_NOCREATE                    = 32
	D_IOS_NOREPLACE                   = 64
	D_IOS_OUTPUT                      = 2
	D_IOS_TRUNC                       = 16
	D_IO_BAD_SEEN                     = 16384
	D_IO_BOOLALPHA                    = 65536
	D_IO_BUFSIZ                       = 8192
	D_IO_CURRENTLY_PUTTING            = 2048
	D_IO_DEC                          = 16
	D_IO_DELETE_DONT_CLOSE            = 64
	D_IO_DONT_CLOSE                   = 32768
	D_IO_EOF_SEEN                     = 16
	D_IO_ERR_SEEN                     = 32
	D_IO_FIXED                        = 4096
	D_IO_FLAGS2_MMAP                  = 1
	D_IO_FLAGS2_NOTCANCEL             = 2
	D_IO_FLAGS2_USER_WBUF             = 8
	D_IO_HAVE_ST_BLKSIZE              = 0
	D_IO_HEX                          = 64
	D_IO_INTERNAL                     = 8
	D_IO_IN_BACKUP                    = 256
	D_IO_IS_APPENDING                 = 4096
	D_IO_IS_FILEBUF                   = 8192
	D_IO_LEFT                         = 2
	D_IO_LINE_BUF                     = 512
	D_IO_LINKED                       = 128
	D_IO_MAGIC                        = 4222418944
	D_IO_MAGIC_MASK                   = 4294901760
	D_IO_NO_READS                     = 4
	D_IO_NO_WRITES                    = 8
	D_IO_OCT                          = 32
	D_IO_RIGHT                        = 4
	D_IO_SCIENTIFIC                   = 2048
	D_IO_SHOWBASE                     = 128
	D_IO_SHOWPOINT                    = 256
	D_IO_SHOWPOS                      = 1024
	D_IO_SKIPWS                       = 1
	D_IO_STDIO                        = 16384
	D_IO_TIED_PUT_GET                 = 1024
	D_IO_UNBUFFERED                   = 2
	D_IO_UNIFIED_JUMPTABLES           = 1
	D_IO_UNITBUF                      = 8192
	D_IO_UPPERCASE                    = 512
	D_IO_USER_BUF                     = 1
	D_IO_USER_LOCK                    = 32768
	D_OLD_STDIO_MAGIC                 = 4206624768
	D_POSIX_C_SOURCE                  = 200809
	D_POSIX_SOURCE                    = 1
	D_SIZET_                          = 0
	D_SIZE_T                          = 0
	D_SIZE_T_                         = 0
	D_SIZE_T_DECLARED                 = 0
	D_SIZE_T_DEFINED                  = 0
	D_SIZE_T_DEFINED_                 = 0
	D_STDARG_H                        = 0
	D_STDC_PREDEF_H                   = 1
	D_STDIO_H                         = 1
	D_STDIO_USES_IOSTREAM             = 0
	D_SYS_CDEFS_H                     = 1
	D_SYS_SIZE_T_H                    = 0
	D_T_SIZE                          = 0
	D_T_SIZE_                         = 0
	D_VA_LIST                         = 0
	D_VA_LIST_                        = 0
	D_VA_LIST_DEFINED                 = 0
	D_VA_LIST_T_H                     = 0
	D__ATOMIC_ACQUIRE                 = 2
	D__ATOMIC_ACQ_REL                 = 4
	D__ATOMIC_CONSUME                 = 1
	D__ATOMIC_HLE_ACQUIRE             = 65536
	D__ATOMIC_HLE_RELEASE             = 131072
	D__ATOMIC_RELAXED                 = 0
	D__ATOMIC_RELEASE                 = 3
	D__ATOMIC_SEQ_CST                 = 5
	D__BEGIN_DECLS                    = 0
	D__BIGGEST_ALIGNMENT__            = 16
	D__BYTE_ORDER__                   = 1234
	D__CHAR_BIT__                     = 8
	D__COUNTER__                      = 0
	D__DBL_DECIMAL_DIG__              = 17
	D__DBL_DIG__                      = 15
	D__DBL_HAS_DENORM__               = 1
	D__DBL_HAS_INFINITY__             = 1
	D__DBL_HAS_QUIET_NAN__            = 1
	D__DBL_MANT_DIG__                 = 53
	D__DBL_MAX_10_EXP__               = 308
	D__DBL_MAX_EXP__                  = 1024
	D__DBL_MIN_10_EXP__               = -307
	D__DBL_MIN_EXP__                  = -1021
	D__DEC128_MANT_DIG__              = 34
	D__DEC128_MAX_EXP__               = 6145
	D__DEC128_MIN_EXP__               = -6142
	D__DEC32_MANT_DIG__               = 7
	D__DEC32_MAX_EXP__                = 97
	D__DEC32_MIN_EXP__                = -94
	D__DEC64_MANT_DIG__               = 16
	D__DEC64_MAX_EXP__                = 385
	D__DEC64_MIN_EXP__                = -382
	D__DECIMAL_BID_FORMAT__           = 1
	D__DECIMAL_DIG__                  = 21
	D__DEC_EVAL_METHOD__              = 2
	D__DI__                           = 0
	D__ELF__                          = 1
	D__END_DECLS                      = 0
	D__FD_SETSIZE                     = 1024
	D__FILE_defined                   = 1
	D__FINITE_MATH_ONLY__             = 0
	D__FLOAT_WORD_ORDER__             = 1234
	D__FLT128_DECIMAL_DIG__           = 36
	D__FLT128_DIG__                   = 33
	D__FLT128_HAS_DENORM__            = 1
	D__FLT128_HAS_INFINITY__          = 1
	D__FLT128_HAS_QUIET_NAN__         = 1
	D__FLT128_MANT_DIG__              = 113
	D__FLT128_MAX_10_EXP__            = 4932
	D__FLT128_MAX_EXP__               = 16384
	D__FLT128_MIN_10_EXP__            = -4931
	D__FLT128_MIN_EXP__               = -16381
	D__FLT32X_DECIMAL_DIG__           = 17
	D__FLT32X_DIG__                   = 15
	D__FLT32X_HAS_DENORM__            = 1
	D__FLT32X_HAS_INFINITY__          = 1
	D__FLT32X_HAS_QUIET_NAN__         = 1
	D__FLT32X_MANT_DIG__              = 53
	D__FLT32X_MAX_10_EXP__            = 308
	D__FLT32X_MAX_EXP__               = 1024
	D__FLT32X_MIN_10_EXP__            = -307
	D__FLT32X_MIN_EXP__               = -1021
	D__FLT32_DECIMAL_DIG__            = 9
	D__FLT32_DIG__                    = 6
	D__FLT32_HAS_DENORM__             = 1
	D__FLT32_HAS_INFINITY__           = 1
	D__FLT32_HAS_QUIET_NAN__          = 1
	D__FLT32_MANT_DIG__               = 24
	D__FLT32_MAX_10_EXP__             = 38
	D__FLT32_MAX_EXP__                = 128
	D__FLT32_MIN_10_EXP__             = -37
	D__FLT32_MIN_EXP__                = -125
	D__FLT64X_DECIMAL_DIG__           = 21
	D__FLT64X_DIG__                   = 18
	D__FLT64X_HAS_DENORM__            = 1
	D__FLT64X_HAS_INFINITY__          = 1
	D__FLT64X_HAS_QUIET_NAN__         = 1
	D__FLT64X_MANT_DIG__              = 64
	D__FLT64X_MAX_10_EXP__            = 4932
	D__FLT64X_MAX_EXP__               = 16384
	D__FLT64X_MIN_10_EXP__            = -4931
	D__FLT64X_MIN_EXP__               = -16381
	D__FLT64_DECIMAL_DIG__            = 17
	D__FLT64_DIG__                    = 15
	D__FLT64_HAS_DENORM__             = 1
	D__FLT64_HAS_INFINITY__           = 1
	D__FLT64_HAS_QUIET_NAN__          = 1
	D__FLT64_MANT_DIG__               = 53
	D__FLT64_MAX_10_EXP__             = 308
	D__FLT64_MAX_EXP__                = 1024
	D__FLT64_MIN_10_EXP__             = -307
	D__FLT64_MIN_EXP__                = -1021
	D__FLT_DECIMAL_DIG__              = 9
	D__FLT_DIG__                      = 6
	D__FLT_EVAL_METHOD_TS_18661_3__   = 2
	D__FLT_EVAL_METHOD__              = 2
	D__FLT_HAS_DENORM__               = 1
	D__FLT_HAS_INFINITY__             = 1
	D__FLT_HAS_QUIET_NAN__            = 1
	D__FLT_MANT_DIG__                 = 24
	D__FLT_MAX_10_EXP__               = 38
	D__FLT_MAX_EXP__                  = 128
	D__FLT_MIN_10_EXP__               = -37
	D__FLT_MIN_EXP__                  = -125
	D__FLT_RADIX__                    = 2
	D__GLIBC_MINOR__                  = 27
	D__GLIBC_USE_DEPRECATED_GETS      = 0
	D__GLIBC_USE_IEC_60559_BFP_EXT    = 0
	D__GLIBC_USE_IEC_60559_FUNCS_EXT  = 0
	D__GLIBC_USE_IEC_60559_TYPES_EXT  = 0
	D__GLIBC_USE_LIB_EXT2             = 0
	D__GLIBC__                        = 2
	D__GNUC_VA_LIST                   = 0
	D__GNU_LIBRARY__                  = 6
	D__GXX_ABI_VERSION                = 1011
	D__HAVE_COLUMN                    = 0
	D__HAVE_GENERIC_SELECTION         = 1
	D__HI__                           = 0
	D__ILP32__                        = 1
	D__INT16_MAX__                    = 32767
	D__INT32_MAX__                    = 2147483647
	D__INT64_MAX__                    = 9223372036854775807
	D__INT8_MAX__                     = 127
	D__INTMAX_MAX__                   = 9223372036854775807
	D__INTMAX_WIDTH__                 = 64
	D__INTPTR_MAX__                   = 2147483647
	D__INTPTR_WIDTH__                 = 32
	D__INT_FAST16_MAX__               = 2147483647
	D__INT_FAST16_WIDTH__             = 32
	D__INT_FAST32_MAX__               = 2147483647
	D__INT_FAST32_WIDTH__             = 32
	D__INT_FAST64_MAX__               = 9223372036854775807
	D__INT_FAST64_WIDTH__             = 64
	D__INT_FAST8_MAX__                = 127
	D__INT_FAST8_WIDTH__              = 8
	D__INT_LEAST16_MAX__              = 32767
	D__INT_LEAST16_WIDTH__            = 16
	D__INT_LEAST32_MAX__              = 2147483647
	D__INT_LEAST32_WIDTH__            = 32
	D__INT_LEAST64_MAX__              = 9223372036854775807
	D__INT_LEAST64_WIDTH__            = 64
	D__INT_LEAST8_MAX__               = 127
	D__INT_LEAST8_WIDTH__             = 8
	D__INT_MAX__                      = 2147483647
	D__INT_WIDTH__                    = 32
	D__KERNEL_STRICT_NAMES            = 0
	D__LDBL_DECIMAL_DIG__             = 21
	D__LDBL_DIG__                     = 18
	D__LDBL_HAS_DENORM__              = 1
	D__LDBL_HAS_INFINITY__            = 1
	D__LDBL_HAS_QUIET_NAN__           = 1
	D__LDBL_MANT_DIG__                = 64
	D__LDBL_MAX_10_EXP__              = 4932
	D__LDBL_MAX_EXP__                 = 16384
	D__LDBL_MIN_10_EXP__              = -4931
	D__LDBL_MIN_EXP__                 = -16381
	D__LINE__                         = 0
	D__LONG_LONG_MAX__                = 9223372036854775807
	D__LONG_LONG_WIDTH__              = 64
	D__LONG_MAX__                     = 2147483647
	D__LONG_WIDTH__                   = 32
	D__NO_INLINE__                    = 1
	D__ORDER_BIG_ENDIAN__             = 4321
	D__ORDER_LITTLE_ENDIAN__          = 1234
	D__ORDER_PDP_ENDIAN__             = 3412
	D__PIC__                          = 2
	D__PIE__                          = 2
	D__PRAGMA_REDEFINE_EXTNAME        = 1
	D__PTRDIFF_MAX__                  = 2147483647
	D__PTRDIFF_WIDTH__                = 32
	D__QI__                           = 0
	D__REGISTER_PREFIX__              = 0
	D__RLIM_T_MATCHES_RLIM64_T        = 0
	D__SCHAR_MAX__                    = 127
	D__SCHAR_WIDTH__                  = 8
	D__SEG_FS                         = 1
	D__SEG_GS                         = 1
	D__SHRT_MAX__                     = 32767
	D__SHRT_WIDTH__                   = 16
	D__SIG_ATOMIC_MAX__               = 2147483647
	D__SIG_ATOMIC_MIN__               = -2147483648
	D__SIG_ATOMIC_WIDTH__             = 32
	D__SIZEOF_DOUBLE__                = 8
	D__SIZEOF_FLOAT128__              = 16
	D__SIZEOF_FLOAT80__               = 12
	D__SIZEOF_FLOAT__                 = 4
	D__SIZEOF_INT__                   = 4
	D__SIZEOF_LONG_DOUBLE__           = 12
	D__SIZEOF_LONG_LONG__             = 8
	D__SIZEOF_LONG__                  = 4
	D__SIZEOF_POINTER__               = 4
	D__SIZEOF_PTRDIFF_T__             = 4
	D__SIZEOF_SHORT__                 = 2
	D__SIZEOF_SIZE_T__                = 4
	D__SIZEOF_WCHAR_T__               = 4
	D__SIZEOF_WINT_T__                = 4
	D__SIZE_MAX__                     = 4294967295
	D__SIZE_T                         = 0
	D__SIZE_T__                       = 0
	D__SIZE_WIDTH__                   = 32
	D__SI__                           = 0
	D__SSP_STRONG__                   = 3
	D__STDC_HOSTED__                  = 1
	D__STDC_IEC_559_COMPLEX__         = 1
	D__STDC_IEC_559__                 = 1
	D__STDC_ISO_10646__               = 201706
	D__STDC_NO_THREADS__              = 1
	D__STDC_UTF_16__                  = 1
	D__STDC_UTF_32__                  = 1
	D__STDC_VERSION__                 = 201112
	D__STDC__                         = 1
	D__THROW                          = 0
	D__THROWNL                        = 0
	D__UINT16_MAX__                   = 65535
	D__UINT32_MAX__                   = 4294967295
	D__UINT64_MAX__                   = 18446744073709551615
	D__UINT8_MAX__                    = 255
	D__UINTMAX_MAX__                  = 18446744073709551615
	D__UINTPTR_MAX__                  = 4294967295
	D__UINT_FAST16_MAX__              = 4294967295
	D__UINT_FAST32_MAX__              = 4294967295
	D__UINT_FAST64_MAX__              = 18446744073709551615
	D__UINT_FAST8_MAX__               = 255
	D__UINT_LEAST16_MAX__             = 65535
	D__UINT_LEAST32_MAX__             = 4294967295
	D__UINT_LEAST64_MAX__             = 18446744073709551615
	D__UINT_LEAST8_MAX__              = 255
	D__USER_LABEL_PREFIX__            = 0
	D__USE_ATFILE                     = 1
	D__USE_FORTIFY_LEVEL              = 0
	D__USE_ISOC11                     = 1
	D__USE_ISOC95                     = 1
	D__USE_ISOC99                     = 1
	D__USE_MISC                       = 1
	D__USE_POSIX                      = 1
	D__USE_POSIX199309                = 1
	D__USE_POSIX199506                = 1
	D__USE_POSIX2                     = 1
	D__USE_POSIX_IMPLICITLY           = 1
	D__USE_XOPEN2K                    = 1
	D__USE_XOPEN2K8                   = 1
	D__WCHAR_MAX__                    = 2147483647
	D__WCHAR_MIN__                    = -2147483648
	D__WCHAR_WIDTH__                  = 32
	D__WINT_MAX__                     = 4294967295
	D__WINT_MIN__                     = 0
	D__WINT_WIDTH__                   = 32
	D__WORDSIZE                       = 32
	D__WORDSIZE32_PTRDIFF_LONG        = 0
	D__WORDSIZE32_SIZE_ULONG          = 0
	D__WORDSIZE_TIME64_COMPAT32       = 0
	D____FILE_defined                 = 1
	D____mbstate_t_defined            = 1
	D___int_size_t_h                  = 0
	D__always_inline                  = 0
	D__attribute_artificial__         = 0
	D__attribute_const__              = 0
	D__attribute_deprecated__         = 0
	D__attribute_malloc__             = 0
	D__attribute_noinline__           = 0
	D__attribute_nonstring__          = 0
	D__attribute_pure__               = 0
	D__attribute_used__               = 0
	D__attribute_warn_unused_result__ = 0
	D__code_model_32__                = 1
	D__extension__                    = 0
	D__glibc_c99_flexarr_available    = 1
	D__i386                           = 1
	D__i386__                         = 1
	D__i686                           = 1
	D__i686__                         = 1
	D__inline                         = 0
	D__linux                          = 1
	D__linux__                        = 1
	D__off_t_defined                  = 0
	D__pentiumpro                     = 1
	D__pentiumpro__                   = 1
	D__pic__                          = 2
	D__pie__                          = 2
	D__restrict                       = 0
	D__size_t                         = 0
	D__size_t__                       = 0
	D__ssize_t_defined                = 0
	D__stub_chflags                   = 0
	D__stub_fattach                   = 0
	D__stub_fchflags                  = 0
	D__stub_fdetach                   = 0
	D__stub_gtty                      = 0
	D__stub_lchmod                    = 0
	D__stub_revoke                    = 0
	D__stub_setlogin                  = 0
	D__stub_sigreturn                 = 0
	D__stub_sstk                      = 0
	D__stub_stty                      = 0
	D__unix                           = 1
	D__unix__                         = 1
	D__va_list__                      = 0
	D__word__                         = 0
	D__wur                            = 0
	Di386                             = 1
	Dlinux                            = 1
	Dunix                             = 1
)

const (
	E__codecvt_error   = 2
	E__codecvt_noconv  = 3
	E__codecvt_ok      = 0
	E__codecvt_partial = 1
)

type S_IO_FILE = struct {
	F_flags          int32
	F_IO_read_ptr    uintptr
	F_IO_read_end    uintptr
	F_IO_read_base   uintptr
	F_IO_write_base  uintptr
	F_IO_write_ptr   uintptr
	F_IO_write_end   uintptr
	F_IO_buf_base    uintptr
	F_IO_buf_end     uintptr
	F_IO_save_base   uintptr
	F_IO_backup_base uintptr
	F_IO_save_end    uintptr
	F_markers        uintptr
	F_chain          uintptr
	F_fileno         int32
	F_flags2         int32
	F_old_offset     int32
	F_cur_column     int16
	F_vtable_offset  int8
	F_shortbuf       [1]int8
	F_lock           uintptr
	F_offset         int64
	F__pad1          uintptr
	F__pad2          uintptr
	F__pad3          uintptr
	F__pad4          uintptr
	F__pad5          int32
	F_mode           int32
	F_unused2        [40]int8
}

type S_IO_marker = struct {
	F_next uintptr
	F_sbuf uintptr
	F_pos  int32
}

var stdio__ [1]byte

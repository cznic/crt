// Copyright 2018 The CRT Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

//TODO go:generate go run generator.go -goos linux -goarch 386
//TODO go:generate go run generator.go -goos linux -goarch amd64
//TODO go:generate go run generator.go -goos linux -goarch arm
//TODO go:generate go run generator.go -goos windows -goarch 386
//TODO go:generate go run generator.go -goos windows -goarch amd64

//TODO(ccgo) Restore src/fenv/x86_64/*.s
//TODO(ccgo) Restore src/internal/x86_64/*.s
//TODO(ccgo) Restore src/math/x86_64/*.s
//TODO(ccgo) Restore src/signal/x86_64/*.s
//TODO(ccgo) Restore src/string/x86_64/*.s
//TODO(ccgo) Restore src/thread/x86_64/*.s
//TODO/ccgo) Restore src/setjmp/x86_64/*.s

//TODO(ccgo) Restore src/fenv/i386/*.s
//TODO(ccgo) Restore src/internal/i386/*.s
//TODO(ccgo) Restore src/math/i386/*.s
//TODO(ccgo) Restore src/signal/i386/*.s
//TODO(ccgo) Restore src/string/i386/*.s
//TODO(ccgo) Restore src/thread/i386/*.s
//TODO/ccgo) Restore src/setjmp/i386/*.s

// Package crt provides C-runtime services. Work In Progress. API unstable.
//
// Installation
//
//     $ go get modernc.org/crt
//
// Documentation: http://godoc.org/modernc.org/crt
//
// The vast majority of this package is a mechanical translation of the musl
// libc project:
//
//	https://www.musl-libc.org/
package crt // import "modernc.org/crt"

import (
	"fmt"
	"os"
	"path/filepath"
	"reflect"
	"runtime"
	"runtime/debug"
	"sort"
	"strings"
	"sync"
	"unsafe"

	"modernc.org/internal/buffer" //TODO-
	"modernc.org/memory"
	"modernc.org/strutil"
)

var (
	allocMu   sync.Mutex
	allocator memory.Allocator
	env       = os.Environ()
	files     = map[uintptr]*os.File{}
	filesMu   sync.Mutex
	logging   bool
	mainTLS   TLS

	Log = func(s string, a ...interface{}) {}

	// RepositoryPath is the path of this repository.
	RepositoryPath string

	// Nz32 holds the float32 value -0.0. R/O
	Nz32 float32

	// Nz64 holds the float64 value -0.0. R/O
	Nz64 float64

	tls0 Thread
)

func init() {
	Nz32 = -Nz32
	Nz64 = -Nz64

	for log := 0; log <= 16; log++ { // Preallocate some mmap pages.
		MustMalloc(1 << uint(log))
	}
	X__libc_start_main(TLS(unsafe.Pointer(&tls0)), 0, 0, 0)

	defer func() {
		Free(tls0.Fstack_page)
		tls0.Fstack_page = 0
	}()

	mainTLS = TLS(X__ccgo_main_tls)
	if (*Thread)(unsafe.Pointer(mainTLS)).Fself != uintptr(mainTLS) { // sanity check
		panic("internal error")
	}

	self, err := strutil.ImportPath()
	if err != nil {
		panic(err)
	}

	gopath := strutil.Gopath()
	for _, v := range strings.Split(gopath, string(os.PathListSeparator)) {
		x := filepath.Join(v, "src", self)
		fi, err := os.Stat(x)
		if err != nil || !fi.IsDir() {
			continue
		}

		RepositoryPath = x
		return
	}

	panic("cannot determine repository path")
}

func Main(main func(TLS, int32, uintptr) int32) {
	if fn := os.Getenv("CCGOLOG"); fn != "" {
		logging = true
		f, err := os.OpenFile(fn, os.O_APPEND|os.O_CREATE|os.O_WRONLY|os.O_SYNC, 0644)
		if err != nil {
			panic(err)
		}

		pid := fmt.Sprintf("[pid %v] ", os.Getpid())

		Log = func(s string, args ...interface{}) {
			if s == "" {
				s = strings.Repeat("%v ", len(args))
			}
			_, fn, fl, _ := runtime.Caller(1)
			s = fmt.Sprintf(pid+"%s:%d: "+s, append([]interface{}{filepath.Base(fn), fl}, args...)...)
			switch {
			case len(s) != 0 && s[len(s)-1] == '\n':
				fmt.Fprint(f, s)
			default:
				fmt.Fprintln(f, s)
			}
		}

		Log("==== start: %v", os.Args)
	}
	tls := MainTLS()
	Xexit(tls, main(tls, int32(len(os.Args)), X__ccgo_argv))
}

func MainTLS() TLS { return mainTLS }

// TLS represents a virtual C thread.
type TLS uintptr

func (tls TLS) Release() { //TODO
	if tls == 0 {
		return
	}

	t := (*Thread)(unsafe.Pointer(tls))
	for t.Fstack_page != 0 {
		h := (*stackPageHeader)(unsafe.Pointer(t.Fstack_page))
		prev := h.prev
		Free(t.Fstack_page)
		t.Fstack_page = uintptr(unsafe.Pointer(prev))
	}
	Free(uintptr(tls))
	return
}

type stackPageHeader struct {
	prev  uintptr
	avail int32
	used  int32
}

func init() {
	if unsafe.Sizeof(stackPageHeader{}) > StackAlign {
		panic("internal error")
	}
}

func MallocStack(tls TLS, size int) (r uintptr) {
	t := (*Thread)(unsafe.Pointer(tls))
	h := (*stackPageHeader)(unsafe.Pointer(t.Fstack_page))
	if h != nil && int(h.avail) >= size {
		r = uintptr(unsafe.Pointer(h)) + uintptr(h.used)
		h.avail -= int32(size)
		h.used += int32(size)
		return r
	}

	rq := size
	if size < stackPage {
		rq = stackPage
	}
	rq += StackAlign - 1
	rq &^= StackAlign - 1

	t.Fstack_page = MustMalloc(rq)
	h0 := h
	h = (*stackPageHeader)(unsafe.Pointer(t.Fstack_page))
	h.prev = uintptr(unsafe.Pointer(h0))
	h.avail = int32(rq - StackAlign)
	h.used = StackAlign

	r = uintptr(unsafe.Pointer(h)) + uintptr(h.used)
	h.avail -= int32(size)
	h.used += int32(size)
	return r
}

func FreeStack(tls TLS, size int) {
	t := (*Thread)(unsafe.Pointer(tls))
	h := (*stackPageHeader)(unsafe.Pointer(t.Fstack_page))
	h.avail += int32(size)
	h.used -= int32(size)
	if h.used > StackAlign || h.prev == 0 {
		return
	}

	t.Fstack_page = h.prev
	Free(uintptr(unsafe.Pointer(h)))
}

// MustMalloc is like Malloc but panics if the allocation cannot be made.
func MustMalloc(size int) uintptr {
	p, err := Malloc(size)
	if err != nil {
		panic(fmt.Errorf("out of memory: %v", err))
	}

	return p
}

// UsableSize reports the size of the memory block allocated at p.
func UsableSize(p uintptr) int {
	allocMu.Lock()
	n := memory.UintptrUsableSize(p)
	allocMu.Unlock()
	return n
}

// Malloc allocates uninitialized memory.
func Malloc(size int) (uintptr, error) {
	allocMu.Lock()
	p, err := allocator.UintptrMalloc(size)
	allocMu.Unlock()
	return p, err
}

// MustCalloc is like Calloc but panics if the allocation cannot be made.
func MustCalloc(size int) uintptr {
	p, err := Calloc(size)
	if err != nil {
		panic(fmt.Errorf("out of memory: %v", err))
	}

	return p
}

// Calloc allocates zeroed memory.
func Calloc(size int) (uintptr, error) {
	allocMu.Lock()
	p, err := allocator.UintptrCalloc(size)
	allocMu.Unlock()
	return p, err
}

// CString allocates a C string initialized from s.
func CString(s string) (uintptr, error) {
	n := len(s)
	p, err := Malloc(n + 1)
	if p == 0 || err != nil {
		return 0, err
	}

	copy((*rawmem)(unsafe.Pointer(p))[:n], s)
	(*rawmem)(unsafe.Pointer(p))[n] = 0
	return p, nil
}

// MustCString is like CString but panic if the allocation cannot be made.
func MustCString(s string) uintptr {
	n := len(s)
	p := MustMalloc(n + 1)
	copy((*rawmem)(unsafe.Pointer(p))[:n], s)
	(*rawmem)(unsafe.Pointer(p))[n] = 0
	return p
}

// BSS allocates the the bss segment of a package/command.
func BSS(init *byte) uintptr {
	r := uintptr(unsafe.Pointer(init))
	if r%unsafe.Sizeof(uintptr(0)) != 0 {
		panic("internal error")
	}

	return r
}

// TS allocates the R/O text segment of a package/command.
func TS(init string) uintptr { return (*reflect.StringHeader)(unsafe.Pointer(&init)).Data }

// Free frees memory allocated by Calloc, Malloc or Realloc.
func Free(p uintptr) error {
	allocMu.Lock()
	err := allocator.UintptrFree(p)
	allocMu.Unlock()
	return err
}

// DS allocates the the data segment of a package/command.
func DS(init []byte) uintptr {
	r := (*reflect.SliceHeader)(unsafe.Pointer(&init)).Data
	if r%unsafe.Sizeof(uintptr(0)) != 0 {
		panic("internal error")
	}

	return r
}

// Copy copies n bytes form src to dest and returns n.
func Copy(dst, src uintptr, n int) int {
	if n != 0 {
		return copy((*rawmem)(unsafe.Pointer(dst))[:n], (*rawmem)(unsafe.Pointer(src))[:n])
	}
	return n
}

// GoString returns a string from a C char* null terminated string s.
func GoString(s uintptr) string {
	if s == 0 {
		return ""
	}

	var b buffer.Bytes
	for {
		ch := *(*byte)(unsafe.Pointer(s))
		if ch == 0 {
			r := string(b.Bytes())
			b.Close()
			return r
		}

		b.WriteByte(ch)
		s++
	}
}

// GoStringLen returns a string from a C char* string s having length len bytes.
func GoStringLen(s uintptr, len int) string {
	if len == 0 {
		return ""
	}

	return string((*rawmem)(unsafe.Pointer(s))[:len])
}

func printAssertFail(expr, file uintptr, line int32, _func uintptr) {
	fmt.Fprintf(os.Stderr, "Assertion failed: %s (%s: %s: %d)\n", GoString(expr), GoString(file), GoString(_func), line)
	fmt.Fprintf(os.Stderr, "%s\n", debug.Stack()) //TODO-
	os.Exit(1)                                    //TODO-
}

func Alloca(p *[]uintptr, n int) uintptr   { r := MustMalloc(n); *p = append(*p, r); return r }
func Preinc(p *uintptr, n uintptr) uintptr { *p += n; return *p }

// Realloc reallocates memory.
func Realloc(p uintptr, size int) (uintptr, error) {
	allocMu.Lock()
	p, err := allocator.UintptrRealloc(p, size)
	allocMu.Unlock()
	return p, err
}

func debugStack() { fmt.Printf("%s\n", debug.Stack()) }

type int16watch = struct {
	s string
	v int16
}

type int32watch = struct {
	s string
	v int32
}

type int64watch = struct {
	s string
	v int64
}

type uint32watch = struct {
	s string
	v uint32
}

type ptrwatch = struct {
	s string
	v uintptr
}

type strwatch = struct {
	s string
	v string
}

var ( //TODO-
	watches = map[uintptr]interface{}{}
	trace   int
)

func WatchPtr(tls TLS, s string, p uintptr) {
	if s == "" {
		delete(watches, p)
		return
	}

	v := *(*uintptr)(unsafe.Pointer(p))
	watches[p] = &ptrwatch{s, v}
	watching(tls, []string{fmt.Sprintf("%q @ %#x is initially %#x", s, p, v)})
}

func WatchInt16(tls TLS, s string, p uintptr) {
	if s == "" {
		delete(watches, p)
		return
	}

	v := *(*int16)(unsafe.Pointer(p))
	watches[p] = &int16watch{s, v}
	watching(tls, []string{fmt.Sprintf("%q @ %#x is initially %v(%#[3]x)", s, p, v)})
}

func WatchInt32(tls TLS, s string, p uintptr) {
	if s == "" {
		delete(watches, p)
		return
	}

	v := *(*int32)(unsafe.Pointer(p))
	watches[p] = &int32watch{s, v}
	watching(tls, []string{fmt.Sprintf("%q @ %#x is initially %v(%#[3]x)", s, p, v)})
}

func WatchInt64(tls TLS, s string, p uintptr) {
	if s == "" {
		delete(watches, p)
		return
	}

	v := *(*int64)(unsafe.Pointer(p))
	watches[p] = &int64watch{s, v}
	watching(tls, []string{fmt.Sprintf("%q @ %#x is initially %v(%#[3]x)", s, p, v)})
}

func WatchString(tls TLS, s string, p uintptr) {
	if s == "" {
		delete(watches, p)
		return
	}

	v := GoString(p)
	watches[p] = &strwatch{s, v}
	watching(tls, []string{fmt.Sprintf("%q @ %#x is initially %q", s, p, v)})
}

func WatchUint32(tls TLS, s string, p uintptr) {
	if s == "" || p == 0 {
		delete(watches, p)
		return
	}

	v := *(*uint32)(unsafe.Pointer(p))
	watches[p] = &uint32watch{s, v}
	watching(tls, []string{fmt.Sprintf("%q @ %#x is initially %v(%#[3]x)", s, p, v)})
}

func watching(tls TLS, a []string) {
	sort.Strings(a)
	Log("%s\n%s----\n", strings.Join(a, "\n"), debug.Stack())
}

func TraceOn()  { trace++ }
func TraceOff() { trace-- }

func Watch(tls TLS) { //TODO-
	if trace > 0 {
		_, fn, fl, _ := runtime.Caller(1)
		Log("# trace %s:%d:\n", filepath.Base(fn), fl)
	}
	var a []string
	for p, v := range watches {
		switch x := v.(type) {
		case *int16watch:
			if w := *(*int16)(unsafe.Pointer(p)); w != x.v {
				x.v = w
				a = append(a, fmt.Sprintf("%q @ %#x is now %v(%#[3]x)", x.s, p, w))
			}
		case *int32watch:
			if w := *(*int32)(unsafe.Pointer(p)); w != x.v {
				x.v = w
				a = append(a, fmt.Sprintf("%q @ %#x is now %v(%#[3]x)", x.s, p, w))
			}
		case *int64watch:
			if w := *(*int64)(unsafe.Pointer(p)); w != x.v {
				a = append(a, fmt.Sprintf("%q @ %#x is now %v(%#[3]x) was %[4]v(%#[4]x)", x.s, p, w, x.v))
				x.v = w
			}
		case *uint32watch:
			if w := *(*uint32)(unsafe.Pointer(p)); w != x.v {
				x.v = w
				a = append(a, fmt.Sprintf("%q @ %#x is now %v(%#[3]x)", x.s, p, w))
			}
		case *ptrwatch:
			if w := *(*uintptr)(unsafe.Pointer(p)); w != x.v {
				x.v = w
				a = append(a, fmt.Sprintf("%q @ %#x is now %#x)", x.s, p, w))
			}
		case *strwatch:
			if w := GoString(p); w != x.v {
				x.v = w
				a = append(a, fmt.Sprintf("%q @ %#x is now %q)", x.s, p, w))
			}
		}
	}
	if len(a) != 0 {
		watching(tls, a)
	}
}

func File(fd uintptr) *os.File {
	if int(fd) < 0 {
		return nil
	}

	filesMu.Lock()

	defer filesMu.Unlock()

	f := files[fd]
	if f == nil {
		f = os.NewFile(fd, fmt.Sprintf("fd%v", fd))
		files[fd] = f
	}
	return f
}

var (
	logBuf  [1 << 10]byte
	logBufp = uintptr(unsafe.Pointer(&logBuf))
)

func X__log(tls TLS, format uintptr, args ...interface{}) {
	if !logging {
		return
	}
	ap := X__builtin_va_start(tls, args)
	n := Xvsnprintf(tls, logBufp, SizeT(len(logBuf)-1), format, ap)
	Log("%s", GoStringLen(logBufp, int(n)))
	X__builtin_va_end(tls, ap)
}

// ctype

func Xisalnum(tls TLS, c int32) (r int32) {
	if c >= 0 && currentLocaleBits[byte(c)]&isalnum != 0 {
		return 1
	}

	return 0
}

func Xisalpha(tls TLS, c int32) (r int32) {
	if c >= 0 && currentLocaleBits[byte(c)]&isalpha != 0 {
		return 1
	}

	return 0
}

func Xisblank(tls TLS, c int32) (r int32) {
	if c >= 0 && currentLocaleBits[byte(c)]&isblank != 0 {
		return 1
	}

	return 0
}

func Xiscntrl(tls TLS, c int32) (r int32) {
	if c >= 0 && currentLocaleBits[byte(c)]&iscntrl != 0 {
		return 1
	}

	return 0
}

func Xisdigit(tls TLS, c int32) (r int32) {
	if c >= 0 && currentLocaleBits[byte(c)]&isdigit != 0 {
		return 1
	}

	return 0
}

func Xisgraph(tls TLS, c int32) (r int32) {
	if c >= 0 && currentLocaleBits[byte(c)]&isgraph != 0 {
		return 1
	}

	return 0
}

func Xislower(tls TLS, c int32) (r int32) {
	if c >= 0 && currentLocaleBits[byte(c)]&islower != 0 {
		return 1
	}

	return 0
}

func X__builtin_isprint(tls TLS, c int32) (r int32) { return Xisprint(tls, c) }

func Xisprint(tls TLS, c int32) (r int32) {
	if c >= 0 && currentLocaleBits[byte(c)]&isprint != 0 {
		return 1
	}

	return 0
}

func Xispunct(tls TLS, c int32) (r int32) {
	if c >= 0 && currentLocaleBits[byte(c)]&ispunct != 0 {
		return 1
	}

	return 0
}

func Xisspace(tls TLS, c int32) (r int32) {
	if c >= 0 && currentLocaleBits[byte(c)]&isspace != 0 {
		return 1
	}

	return 0
}

func Xisupper(tls TLS, c int32) (r int32) {
	if c >= 0 && currentLocaleBits[byte(c)]&isupper != 0 {
		return 1
	}

	return 0
}

func Xisxdigit(tls TLS, c int32) (r int32) {
	if c >= 0 && currentLocaleBits[byte(c)]&isxdigit != 0 {
		return 1
	}

	return 0
}

func Xtolower(tls TLS, c int32) (r int32) {
	if c >= 0 {
		return int32(currentLocaleToLower[byte(c)])
	}

	return c
}

func Xtoupper(tls TLS, c int32) (r int32) {
	if c >= 0 {
		return int32(currentLocaleToUpper[byte(c)])
	}

	return c
}

func setCurrentLocale(s string) {
	switch s {
	case "", "POSIX":
		s = "C"
	}
	if p := ctypeBits[s]; p != nil {
		currentLocaleBits = p
	}
	if p := ctypeToLower[s]; p != nil {
		currentLocaleToLower = p
	}
	if p := ctypeToUpper[s]; p != nil {
		currentLocaleToUpper = p
	}
}

func X__ccgo_arg(tls TLS, i int32) uintptr { return MustCString(os.Args[i]) }
func X__ccgo_argc(tls TLS) int32           { return int32(len(os.Args)) }
func X__ccgo_env(tls TLS, i int32) uintptr { return MustCString(env[i]) }
func X__ccgo_envc(tls TLS) int32           { return int32(len(env)) }
